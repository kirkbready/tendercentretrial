﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TenderCentreTrial {
    public class UserSql {

        public static string Insert {
            get {
                return @"insert dbo.AspNetUsers(CountryId, CustomerId, Rf, [Version], UserName, PasswordHash, SecurityStamp, AccessFailedCount, LockoutEnabled, LockoutEndDate, Discriminator, UpdatedOn, UpdatedBy, CreatedOn, CreatedBy) 
values (@CountryId, @CustomerId, @Rf, @Version, @UserName, @PasswordHash, @SecurityStamp, @AccessFailedCount, @LockoutEnabled, @LockoutEndDate, @Discriminator, @UpdatedOn, @UpdatedBy, @CreatedOn, @CreatedBy)

select cast(scope_identity() as int)";
            }
        }

        public static string Update {
            get {
                return @"update dbo.AspNetUsers set
    Rf = @Rf,
    [Version] = [Version]+1,
    UserName = @UserName,
    PasswordHash = @PasswordHash,
    SecurityStamp = @SecurityStamp,
    AccessFailedCount = @AccessFailedCount,
    LockoutEnabled = @LockoutEnabled,
    LockoutEndDate = @LockoutEndDate,
    Discriminator = @Discriminator,
    UpdatedOn = @UpdatedOn,
    UpdatedBy = @UpdatedBy,
    CreatedOn = @CreatedOn,
    CreatedBy = @CreatedBy
where CountryId = @CountryId and CustomerId = @CustomerId and Id = @Id";
            }
        }

        public static string Delete
        {
            get
            {
                return @"delete from dbo.AspNetUsers where Id = @Id" ;
            }
        }

        public static string GetByName
        {
            get
            {
                return @"select CountryId, CustomerId, Id, Version, UserName, PasswordHash, SecurityStamp, AccessFailedCount, LockoutEnabled, LockoutEndDate, Discriminator, UpdatedOn, UpdatedBy, CreatedOn, CreatedBy from dbo.AspNetUsers where UserName = @UserName";
            }
        }
        public static string GetById
        {
            get
            {
                return @"select CountryId, CustomerId, Id, Version, UserName, PasswordHash, SecurityStamp, AccessFailedCount, LockoutEnabled, LockoutEndDate, Discriminator, UpdatedOn, UpdatedBy, CreatedOn, CreatedBy from dbo.AspNetUsers where Id = @Id";
            }
        }

        public static string GetPasswordHash
        {
            get
            {
                return @"select PasswordHash from dbo.AspNetUsers where Id = @Id";
            }
        }


    }

    public class UserClaimSql {

        public static string Insert {
            get {
                return @"insert dbo.AspNetUserClaims(CountryId, CustomerId, UserId, [Version], ClaimType, ClaimValue, UpdatedOn, UpdatedBy, CreatedOn, CreatedBy) 
values (@CountryId, @CustomerId, @UserId, @Version, @ClaimType, @ClaimValue, @UpdatedOn, @UpdatedBy, @CreatedOn, @CreatedBy)

select cast(scope_identity() as int)";
            }
        }

        public static string Update {
            get {
                return @"update dbo.AspNetUserClaims set
    CountryId = @CountryId,
    CustomerId = @CustomerId,
    UserId = @UserId,
    [Version] = [Version]+1,
    ClaimType = @ClaimType,
    ClaimValue = @ClaimValue,
    UpdatedOn = @UpdatedOn,
    UpdatedBy = @UpdatedBy,
    CreatedOn = @CreatedOn,
    CreatedBy = @CreatedBy
    where Id = @Id";
            }
        }

        public static string GetClaimByUser
        {
            get
            {
                return @"select CountryId, CustomerId, UserId, Id, Version, ClaimType, ClaimValue, UpdatedOn, UpdatedBy, CreatedOn, CreatedBy from dbo.AspNetUserClaims where CountryId = @CountryId and CustomerId = @CustomerId and UserId = @UserId";
            }
        }
    }
}
