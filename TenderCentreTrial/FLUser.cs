﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;

namespace TenderCentreTrial {

    public class FLUser : FLUserBase<FLUserClaim> {
        public override int Id { get; set; }

        public void AddClaim(FLUserClaim claim) {
            claim.User = this;
            claim.CountryId = this.CountryId;
            claim.CustomerId = this.CustomerId;
            Claims.Add(claim);
        }

        public void RemoveClaim(FLUserClaim claim) {
            claim.User = null;
            Claims.Remove(claim);
        }
    }

    public abstract class FLUserBase<T> : IUser<int>, IFLUser<T> {
        public int CountryId { get; set; }
        public int CustomerId { get; set; }
        public abstract int Id { get; set; }
        public int Version { get; set; }
        public string Rf { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public int AccessFailedCount { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTime LockoutEndDate { get; set; }
        public string Discriminator { get; set; }
        public IList<T> Claims { get; protected set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public FLUserBase() {
            Claims = new List<T>();

            Version = 1;
            Discriminator = "FLUser";
            PasswordHash = SecurityStamp = string.Empty;
            LockoutEndDate = (DateTime)SqlDateTime.MinValue;
            Rf = string.Empty;
            UpdatedOn = CreatedOn = DateTime.UtcNow;
            UpdatedBy = CreatedBy = string.Empty;
        }
    }
}
