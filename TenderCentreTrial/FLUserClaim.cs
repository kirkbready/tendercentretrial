﻿using System;

namespace TenderCentreTrial {

    public class FLUserClaim : FLUserClaimBase {
        public FLUser User { get; set; }

        public FLUserClaim()
            : base()
        {
        }

        public FLUserClaim(string claimType, string ClaimValue) : base() {
            this.ClaimType = claimType;
            this.ClaimValue = ClaimValue;
        }
    }

    public class FLUserClaimBase {
        public int CountryId { get; set; }
        public int CustomerId { get; set; }
        public int Id { get; set; }
        public int Version { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public FLUserClaimBase() {
            Version = 1;

            UpdatedOn = CreatedOn = DateTime.Now;
            UpdatedBy = CreatedBy = string.Empty;
        }
    }
}
