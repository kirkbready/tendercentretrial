﻿using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;

namespace TenderCentreTrial
{
    public class FLUserStore<TUser> : IUserStore<TUser, int>, IUserPasswordStore<TUser, int>,
        IUserSecurityStampStore<TUser, int>, IUserLockoutStore<TUser, int> where TUser : FLUser {

        string cnnString; 


        public FLUserStore(string cnnString) {
            this.cnnString = cnnString;
        }

        /// <summary>
        /// Insert a new user in the AspNetUsers table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Task CreateAsync(TUser user) {

                using (var cnn = new SqlConnection(cnnString)) {
                    cnn.Open();

                    using (var tx = cnn.BeginTransaction()) {
                    user.Id = cnn.QueryAsync<int>(UserSql.Insert, user, tx).Result.Single();

                        foreach (var claim in user.Claims) {
                        claim.Id = cnn.QueryAsync<int>(UserClaimSql.Insert, new {
                                CountryId = claim.CountryId,
                                CustomerId = claim.CustomerId,
                                UserId = claim.User.Id,
                                Version = claim.Version,
                                ClaimType = claim.ClaimType,
                                ClaimValue = claim.ClaimValue,
                                UpdatedOn = claim.UpdatedOn,
                                UpdatedBy = claim.UpdatedBy,
                                CreatedOn = claim.CreatedOn,
                                CreatedBy = claim.CreatedBy

                        }, tx).Result.Single();
                        }

                        tx.Commit();
                    }
                }

            return Task.FromResult<TUser>(user);
        }

        public Task DeleteAsync(TUser user) {
            var t = Task.Factory.StartNew(() =>
            {
                using (var cnn = new SqlConnection(cnnString))
                {
                    cnn.Open();

                    using (var tx = cnn.BeginTransaction())
                    {
                        cnn.Execute(UserSql.Delete,new{Id = user.Id}, tx);
                        tx.Commit();
                    }
                }
            });
            Task.WaitAll(t);
            return Task.FromResult<TUser>(user);

            return Task.FromResult<Object>(null); 
        }

        public Task<TUser> FindByIdAsync(int userId) {
            if (userId <= 0 )
            {
                throw new ArgumentException("Null or empty argument: userId");
            }
            TUser user = null;
            var t = Task.Factory.StartNew(() =>
            {
                using (var cnn = new SqlConnection(cnnString))
                {
                    cnn.Open();

                    using (var tx = cnn.BeginTransaction())
                    {
                        user = cnn.Query<TUser>(UserSql.GetById, new { Id = userId }, tx).SingleOrDefault();
                        if (user != null)
                        {
                            var claims = cnn.Query<FLUserClaim>(UserClaimSql.GetClaimByUser,
                                new { CountryId = user.CountryId, CustomerId = user.CustomerId, UserId = user.Id }, tx);
                            foreach (var claim in claims)
                            {
                                user.AddClaim(claim);
                            }
                            tx.Commit();
                        }
                    }
                }
            });
            Task.WaitAll(t);
            return Task.FromResult<TUser>(user);
        }

        /// <summary>
        /// Returns an TUser instance based on a userName query 
        /// </summary>
        /// <param name="userName">The user's name</param>
        /// <returns>Return user instance of the given user. Return null if no matching user exist.</returns>
        public Task<TUser> FindByNameAsync(string userName) {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentException("Null or empty argument: userName");
            }
            TUser user = null;
            var t = Task.Factory.StartNew(() =>
            {
                using (var cnn = new SqlConnection(cnnString))
                {
                    cnn.Open();

                    using (var tx = cnn.BeginTransaction())
                    {
                        user = cnn.Query<TUser>(UserSql.GetByName, new { UserName = userName }, tx).SingleOrDefault();
                        if (user != null)
                        {
                            var claims = cnn.Query<FLUserClaim>(UserClaimSql.GetClaimByUser,
                                new {CountryId = user.CountryId, CustomerId = user.CustomerId, UserId = user.Id}, tx);
                            foreach (var claim  in claims)
                            {
                                user.AddClaim(claim);
                            }
                            tx.Commit();
                        }
                    }
                }
            });
            Task.WaitAll(t);
            return Task.FromResult<TUser>(user);
         
        }

        public Task UpdateAsync(TUser user) {
            var t = Task.Factory.StartNew(() =>
            {
                using (var cnn = new SqlConnection(cnnString))
                {
                    cnn.Open();

                    using (var tx = cnn.BeginTransaction())
                    {
                        cnn.Execute(UserSql.Update, user, tx);

                        foreach (var claim in user.Claims)
                        {
                            cnn.Execute(UserClaimSql.Update, new
                            {
                                Id = claim.Id,
                                CountryId = claim.CountryId,
                                CustomerId = claim.CustomerId,
                                UserId = claim.User.Id,
                                //--Version removed from parameter list because of incrementing the version in query itself--
                               // Version = claim.Version,  
                                ClaimType = claim.ClaimType,
                                ClaimValue = claim.ClaimValue,
                                UpdatedOn = claim.UpdatedOn,
                                UpdatedBy = claim.UpdatedBy,
                                CreatedOn = claim.CreatedOn,
                                CreatedBy = claim.CreatedBy
                            }, tx);
                        }
                        tx.Commit();
                    }
                }
            });
            Task.WaitAll(t);
            return Task.FromResult<TUser>(user);
        }

        public Task<string> GetPasswordHashAsync(TUser user) {

            if (user == null)
            {
                throw new ArgumentException("Null or empty argument: userName");
            }
            string passwordHash=null;
            var t = Task.Factory.StartNew(() =>
            {
                using (var cnn = new SqlConnection(cnnString))
                {
                    cnn.Open();

                    using (var tx = cnn.BeginTransaction())
                    {
                         passwordHash = cnn.ExecuteScalar(UserSql.GetPasswordHash, new { Id= user.Id}, tx).ToString();
                    }
                }
            });
            Task.WaitAll(t);
            return Task.FromResult<string>(passwordHash); 
            
         
        }

        public Task<bool> HasPasswordAsync(TUser user)
        {
                var passwordHash = GetPasswordHashAsync(user);
                var hasPassword = !string.IsNullOrEmpty(passwordHash.Result.ToString());
            return Task.FromResult<bool>(hasPassword);
        }

        public Task SetPasswordHashAsync(TUser user, string passwordHash) {
            user.PasswordHash = passwordHash;
            UpdateAsync(user);
            return Task.FromResult<Object>(null);
        }

        public Task<string> GetSecurityStampAsync(TUser user) {
            return Task.FromResult(user.SecurityStamp);
        }

        public Task SetSecurityStampAsync(TUser user, string stamp) {
            user.SecurityStamp = stamp;

            return Task.FromResult(0);
        }

        public Task<int> GetAccessFailedCountAsync(TUser user) {
            return Task.FromResult(user.AccessFailedCount);
        }

        public Task<bool> GetLockoutEnabledAsync(TUser user) {
            return Task.FromResult(user.LockoutEnabled);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
        {
            return
                Task.FromResult(new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDate, DateTimeKind.Utc)));
        }

        public Task<int> IncrementAccessFailedCountAsync(TUser user) {
            user.AccessFailedCount++;
            UpdateAsync(user);

            return Task.FromResult(user.AccessFailedCount);
             
        }

        public Task ResetAccessFailedCountAsync(TUser user) {
            user.AccessFailedCount = 0;
            UpdateAsync(user);

            return Task.FromResult(0);
        }

        public Task SetLockoutEnabledAsync(TUser user, bool enabled) {
            user.LockoutEnabled = enabled;
            UpdateAsync(user);

            return Task.FromResult(0);
        }

        public Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd) {
            user.LockoutEndDate = lockoutEnd.UtcDateTime;
            UpdateAsync(user);

            return Task.FromResult(0);
        }

        public void Dispose()
        {
            //No objects to dispose 
        }
    }
}
