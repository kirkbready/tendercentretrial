﻿using System;
using System.Collections.Generic;

namespace TenderCentreTrial {

    public interface IFLUser<T> {
        int CountryId { get; set; }
        int Id { get; set; }
        string Rf { get; set; }
        string PasswordHash { get; set; }
        string SecurityStamp { get; set; }
        int AccessFailedCount { get; set; }
        bool LockoutEnabled { get; set; }
        DateTime LockoutEndDate { get; set; }
        string Discriminator { get; set; }
        IList<T> Claims { get; }
    }

}
