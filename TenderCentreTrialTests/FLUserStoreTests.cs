﻿using System;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using TenderCentreTrial;
using Xunit;

namespace TenderCentreTrialTests {

    public class FLUserStoreTests
    {

        private FLUserStore<FLUser> store;

        public FLUserStoreTests()
        {
            store = new FLUserStore<FLUser>(ConfigurationManager.ConnectionStrings["cnn"].ConnectionString);
        }


        public FLUser GetTestUser()
        {
            var retVal = store.FindByNameAsync("kirk@test.com");
            if (retVal.Result != null)
            {
                return retVal.Result;
            }
            else
            {

                var user = new FLUser
                {
                    CountryId = 1,
                    CustomerId = 1,
                    Rf = "SYD",
                    UserName = "kirk@test.com"
                };

                user.AddClaim(new FLUserClaim(ClaimTypes.Name, "Kirk Bready"));
                user.AddClaim(new FLUserClaim(ClaimTypes.Country, "Australia"));
                user.AddClaim(new FLUserClaim(ClaimTypes.DateOfBirth, "01/01/2000"));
                store.CreateAsync(user);
                return user;
            }
        }

        [Fact]
        public void CanCreateNewUser()
        {

            var user = new FLUser
            {
                CountryId = 1,
                CustomerId = new Random().Next(1000),
                Rf = "SYD",
                UserName = "kirk"+new Random().Next(1000)+"@test.com"
            };

            user.AddClaim(new FLUserClaim(ClaimTypes.Name, "Kirk Bready"));
            user.AddClaim(new FLUserClaim(ClaimTypes.Country, "Australia"));
            user.AddClaim(new FLUserClaim(ClaimTypes.DateOfBirth, "01/01/2000"));

            var retVal = store.CreateAsync(user);

            Assert.NotEqual(0, user.Id);
            foreach (var claim in user.Claims)
                Assert.NotEqual(0, claim.Id);

        }

        [Fact]
        public void CanFindByName()
        {
            //Get Test User 
            var user = GetTestUser();

            var retVal = store.FindByNameAsync("kirk@test.com");

            Assert.NotEqual(0, retVal.Result.Id);
            foreach (var claim in retVal.Result.Claims)
                Assert.NotEqual(0, claim.Id);
        }


        [Fact]
        public void CanFindById()
        {
            var retVal = store.FindByIdAsync(GetTestUser().Id);

            Assert.NotEqual(0, retVal.Id);
            foreach (var claim in retVal.Result.Claims)
                Assert.NotEqual(0, claim.Id);
        }


        [Fact]
        public void CanFindByNameInvalidUser()
        {
            var retVal = store.FindByNameAsync("invalid user");

            Assert.Equal(null, retVal.Result);
             
        }

        [Fact]
        public void CanDeleteUser()
        {
            //Create user to check the delete process 
            var user = new FLUser
            {
                CountryId = 999,
                CustomerId = 999,
                Rf = "SYD",
                UserName = "delete@test.com"
            };

            user.AddClaim(new FLUserClaim(ClaimTypes.Name, "User to Delete"));
            user.AddClaim(new FLUserClaim(ClaimTypes.Country, "Australia"));
            user.AddClaim(new FLUserClaim(ClaimTypes.DateOfBirth, "01/01/2000"));
            var newUser = store.CreateAsync(user);
            Assert.NotEqual(0, user.Id);

            //Check newly created user exist
            var userToDelete = store.FindByNameAsync("delete@test.com");
            Assert.Equal(user.Id, userToDelete.Result.Id);

            //Delete the newly created user
            store.DeleteAsync(user);

            //Find user after deleting and confirm it return null value
            var deletedUser = store.FindByNameAsync("delete@test.com");
            Assert.Equal(null, deletedUser.Result); 

        }

        [Fact]
        public void CanUpdate()
        {
            //Get Test User Details
            var user = GetTestUser();

            
            //Update user information
            var userClaim = user.Claims.Single(claim => claim.ClaimType == ClaimTypes.DateOfBirth);
            userClaim.ClaimValue = "02/02/2000";
            DateTime updatDateTime = DateTime.UtcNow;
            int claimVersion = userClaim.Version;
            user.UpdatedBy = "TestUser";
            store.UpdateAsync(user);

            //Get test user details
            var retVal = store.FindByIdAsync(1);
            user = retVal.Result;
            userClaim = user.Claims.Single(claim => claim.ClaimType == ClaimTypes.DateOfBirth);

            //Check whether new details updated
            Assert.Equal("TestUser", user.UpdatedBy); 
            Assert.Equal(claimVersion+1, userClaim.Version); 
        }

        [Fact]
        public void CanGetPassword()
        {
            //Get Test User Details
            var user = GetTestUser();

             
            var retValPasswordHash = store.GetPasswordHashAsync(user);
           // Assert.NotEqual(string.Empty, retValPasswordHash.Result);
            Assert.Equal(user.PasswordHash, retValPasswordHash.Result);
 
        }

        [Fact]
        public void CheckPasswordHashMethods()
        {
            //Get Test User Details
            var user = GetTestUser();


            store.SetPasswordHashAsync(user,"passwordhash_test_text");
            var hasPasswordResult = store.HasPasswordAsync(user);
            Assert.Equal(true, hasPasswordResult.Result);

        }

        public void CheckSecurityStamp()
        {
            //Since teh method return the value from the object property, no unit test method included. 
        }

        [Fact]
        public void CheckAccessFailedMethod()
        {
            //Get Test User Details
            var user = GetTestUser();

            //Reset the failed access count 
            store.ResetAccessFailedCountAsync(user);

            //Check the failed access count is 0 after resetting
            var failedCountResult = store.GetAccessFailedCountAsync(user);
            int failedCount = failedCountResult.Result;
            Assert.Equal(0, failedCount);

            //Increment the failed access count
            store.IncrementAccessFailedCountAsync(user);

            //Check the failed access count is 1 after incrementing
            failedCountResult = store.GetAccessFailedCountAsync(user);
            failedCount = failedCountResult.Result;
            Assert.Equal(1, failedCount);
             
        }

        [Fact]
        public void CheckLockOutMethods()
        {
            //Get Test User Details
            var user = GetTestUser();
            DateTime lockoutDateTime = DateTime.UtcNow;
            store.SetLockoutEnabledAsync(user, true);
            store.SetLockoutEndDateAsync(user, new DateTimeOffset(lockoutDateTime));

            user = GetTestUser();

            bool lockOutEnabled = store.GetLockoutEnabledAsync(user).Result;
            Assert.Equal(true, lockOutEnabled);

            DateTimeOffset lockoutDateTimeOffset = new DateTimeOffset(DateTime.SpecifyKind(lockoutDateTime, DateTimeKind.Utc));
            DateTimeOffset userLockoutDateTimeOffset = store.GetLockoutEndDateAsync(user).Result;
            Assert.Equal(lockoutDateTimeOffset.ToString("yyyy-MM-dd HH:mm:ss"), userLockoutDateTimeOffset.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        ///Used to check the Team City Build step Run xUnit test with failed test  
        // [Fact]
        public void SampleFailedTest()
        {
            //Get Test User 
            var user = GetTestUser();

            var retVal = store.FindByNameAsync("kirk@test.com");

            Assert.NotEqual(0, retVal.Result.Id);
            foreach (var claim in retVal.Result.Claims)
                Assert.Equal(0, claim.Id);
        }
 

    } 
}
