-- Note: Other user attributes stored as claims
--       (see  System.Security.Claims.ClaimTypes enum)
create table dbo.AspNetUsers (
	Id int identity(1,1) not null,
	CountryId int not null,
	CustomerId int not null,
	Rf nvarchar(3) not null,
	[Version] int not null,
	UserName nvarchar(100) not null,
	PasswordHash nvarchar(max) not null,
	SecurityStamp nvarchar(max) not null,
	AccessFailedCount int not null,
	LockoutEnabled bit not null,
	LockoutEndDate DateTime not null,
	Discriminator nvarchar(128) not null,
	UpdatedOn dateTime not null,
	UpdatedBy nvarchar(50) not null,
	CreatedOn dateTime not null,
	CreatedBy nvarchar(50) not null
)

alter table dbo.AspNetUsers
	add constraint PK_AspNetUsers primary key nonclustered (Id)

create unique clustered index IX_AspNetUsers_CountryIdCustomerId on dbo.AspNetUsers(CountryId, CustomerId)
create unique index IX_AspNetUsers_Username on dbo.AspNetUsers(Username)


create table dbo.AspNetUserClaims (
	Id int identity(1,1) not null,
	UserId int not null,
	[Version] int not null,
	ClaimType nvarchar(100) not null,
	ClaimValue nvarchar(max) not null,
	UpdatedOn dateTime not null,
	UpdatedBy nvarchar(50) not null,
	CreatedOn dateTime not null,
	CreatedBy nvarchar(50) not null
)

alter table dbo.AspNetUserClaims 
	add constraint PK_AspNetUserClaims primary key (Id)

alter table dbo.AspNetUserClaims 
	add constraint FK_AspNetUserClaims_AspNetUsers foreign key (UserId) references dbo.AspNetUsers(Id) on delete cascade

create index IX_AspNetUserClaims_UserId on dbo.AspNetUserClaims(UserId)